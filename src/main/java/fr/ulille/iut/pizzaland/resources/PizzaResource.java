package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;
import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

@Produces("application/json")
@Path("/pizzas")
public class PizzaResource {
    private static final Logger LOGGER = Logger.getLogger(PizzaResource.class.getName());

    private PizzaDao pizzas;

    @Context
    public UriInfo uriInfo;

    public PizzaResource() {
        pizzas = BDDFactory.buildDao(PizzaDao.class);
        pizzas.createTable();
        pizzas.createAssocTable();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<PizzaDto> getAll() {
        LOGGER.info("PizzaResource:getAll");

        List<PizzaDto> l = pizzas.getAll().stream().map(Pizza::toDto).collect(Collectors.toList());
        LOGGER.info(l.toString());
        return l;
    }

    @GET
    @Path("{id}")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public PizzaDto getOnePizza(@PathParam("id") UUID id) {
		LOGGER.info("getOnePizza(" + id + ")");
		try {
			Pizza pizza = pizzas.findById(id);
			LOGGER.info(pizza.toString());
			return Pizza.toDto(pizza);
		} catch (Exception e) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}
    
    @POST
    public Response createPizza(PizzaCreateDto PizzaCreateDto) {
        Pizza existing = pizzas.findByName(PizzaCreateDto.getName());
        if (existing != null) {
            throw new WebApplicationException(Response.Status.CONFLICT);
        }

        try {
            Pizza pizza = Pizza.fromPizzaCreateDto(PizzaCreateDto);
            pizzas.insert(pizza);
            PizzaDto PizzaDto = Pizza.toDto(pizza);

            URI uri = uriInfo.getAbsolutePathBuilder().path(pizza.getId().toString()).build();

            return Response.created(uri).entity(PizzaDto).build();
        } catch (Exception e) {
            e.printStackTrace();
            throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
        }

    }
    
    @PUT
    @Path("{id}")
    @Consumes("application/x-www-form-urlencoded")
    public Response addTopping(@PathParam("id") UUID id, @FormParam("name") String name) {
    	Pizza existing = pizzas.findById(id);
        if (existing == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        try {
        	
	        IngredientDao ingdao = BDDFactory.buildDao(IngredientDao.class);
	        Ingredient ing = ingdao.findByName(name);
	        LOGGER.info("Ingredient found : "+ing.toString());
	        existing.addTopping(ing);
	        pizzas.addIngredient(id, ing.getId());
	        LOGGER.info("Associated pizza "+existing.toString()+" with topping "+ing.toString());
	        return Response.ok().build();
	        
        }catch(Exception e) {
        	LOGGER.info(e.getMessage());
        	throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
    }
    
    @DELETE
    @Path("{id}")
    public Response deletePizza(@PathParam("id") UUID id) {
      if ( pizzas.findById(id) == null ) {
        throw new WebApplicationException(Response.Status.NOT_FOUND);
      }

      pizzas.remove(id);

      return Response.status(Response.Status.ACCEPTED).build();
    }

    @GET
    @Path("{id}/name")
    public String getPizzaName(@PathParam("id") UUID id) {
        Pizza Pizza = pizzas.findById(id);

        if (Pizza == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

        return Pizza.getName();
    }

    @POST
    @Consumes("application/x-www-form-urlencoded")
    public Response createPizza(@FormParam("name") String name) {
      Pizza existing = pizzas.findByName(name);
      if (existing != null) {
        throw new WebApplicationException(Response.Status.CONFLICT);
      }

      try {
        Pizza pizza = new Pizza();
        pizza.setName(name);

        pizzas.insert(pizza);

        PizzaDto pizzaDto = Pizza.toDto(pizza);

        URI uri = uriInfo.getAbsolutePathBuilder().path("" + pizza.getId()).build();

        return Response.created(uri).entity(pizzaDto).build();
      } catch (Exception e) {
          e.printStackTrace();
          throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
      }
    }

}

