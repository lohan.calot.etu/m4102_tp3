package fr.ulille.iut.pizzaland.beans;

import java.util.ArrayList;
import java.util.UUID;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.IngredientCreateDto;
import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
	
	private UUID id = UUID.randomUUID();
	private String name;
	private ArrayList<Ingredient> ingredients;
	
	public Pizza(UUID id, String name, ArrayList<Ingredient> ingredients) {
		this.id = id;
		this.name = name;
		this.ingredients = ingredients;
	}
	
	public Pizza() {
		this.name="undefined";
		this.ingredients = new ArrayList<>();
	}
	
	public String toString() {
		StringBuilder res = new StringBuilder("Pizza [id:"+id+", name:"+name+", ingredients:");
		res.append(ingredients.toString()+"]");
		return res.toString();
	}
	
	public boolean equals(Object o) {
		if(o instanceof Pizza) {
			Pizza otherPizza = (Pizza) o;
			return this.id.equals(otherPizza.getId()) || this.name.equals(otherPizza.getName());
		}
		return false;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(ArrayList<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}
	
	public static PizzaDto toDto(Pizza i) {
        PizzaDto dto = new PizzaDto();
        dto.setId(i.getId());
        dto.setName(i.getName());
        dto.setIngredients(i.getIngredients());

        return dto;
    }
    
    public static Pizza fromDto(PizzaDto dto) {
        Pizza pizza = new Pizza();
        pizza.setId(dto.getId());
        pizza.setName(dto.getName());

        return pizza;
    }
    
    public static PizzaCreateDto toCreateDto(Pizza ingredient) {
        PizzaCreateDto dto = new PizzaCreateDto();
        dto.setName(ingredient.getName());
            
        return dto;
    }
    	
    public static Pizza fromPizzaCreateDto(PizzaCreateDto dto) {
        Pizza pizza = new Pizza();
        pizza.setName(dto.getName());
        return pizza;
    }
      
    public boolean addTopping(Ingredient ing) {
    	return ingredients.add(ing);
    }
}
