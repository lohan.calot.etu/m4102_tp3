package fr.ulille.iut.pizzaland.dao;

import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {

    @SqlUpdate("CREATE TABLE IF NOT EXISTS pizzas (id VARCHAR(128) PRIMARY KEY, name VARCHAR UNIQUE NOT NULL);")
    void createTable();
    
    @SqlUpdate("CREATE TABLE IF NOT EXISTS assoc_pizzas_ingredients "
    		+ "(pizza_id VARCHAR(128), ingredient_id VARCHAR(128), PRIMARY KEY (pizza_id, ingredient_id),"
    		+ "FOREIGN KEY (pizza_id) REFERENCES pizzas(id) ON UPDATE CASCADE ON DELETE CASCADE, "
    		+ "FOREIGN KEY (ingredient_id) REFERENCES ingredients(id) ON UPDATE CASCADE ON DELETE CASCADE);")
    void createAssocTable();

    @SqlUpdate("DROP TABLE IF EXISTS pizzas; DROP TABLE IF EXISTS ingredients;")
    void dropTable();

    @SqlUpdate("INSERT INTO pizzas (id, name) VALUES (:id, :name)")
    void insert(@BindBean Pizza pizza);
    
    @SqlUpdate("INSERT INTO assoc_pizzas_ingredients (pizza_id,ingredient_id) VALUES (:piz_id, :ing_id);")
    void addIngredient(@Bind("piz_id") UUID piz_id, @Bind("ing_id") UUID ing_id);

    @SqlQuery("SELECT * FROM pizzas;")
    @RegisterBeanMapper(Pizza.class)
    List<Pizza> getAll();

    @SqlQuery("SELECT * FROM pizzas WHERE id = :id")
    @RegisterBeanMapper(Pizza.class)
    Pizza findById(@Bind("id") UUID id);
    
    @SqlQuery("SELECT * FROM pizzas WHERE name = :name")
    @RegisterBeanMapper(Pizza.class)
    Pizza findByName(@Bind("name") String name);
    
    //@SqlQuery("SELECT * FROM ingredients AS i INNER JOIN assoc_pizzas_ingredients AS api ON i.:id=api.ingredient_id AND api.pizza_id=piz_id;")
    @SqlQuery("SELECT * FROM ingredients WHERE id IN (SELECT ingredient_id FROM PizzaIngredientsAssociation WHERE pizza_id = :piz_id);")
    @RegisterBeanMapper(Ingredient.class)
    List<Ingredient> getPizzaIngredientsById(@Bind("piz_id") UUID id);
    
    @SqlUpdate("DELETE FROM pizzas WHERE id = :id")
    void remove(@Bind("id") UUID id);
}