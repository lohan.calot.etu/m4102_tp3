package fr.ulille.iut.pizzaland;

import fr.ulille.iut.pizzaland.ApiV1;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

/*
 * JerseyTest facilite l'écriture des tests en donnant accès aux
 * méthodes de l'interface jakarta.ws.rs.client.Client.
 * la méthode configure() permet de démarrer la ressource à tester
 */
public class PizzaResourceTest extends JerseyTest {
    private static final Logger LOGGER = Logger.getLogger(PizzaResourceTest.class.getName());
    private PizzaDao dao;

    @Override
    protected Application configure() {
        return new ApiV1();
    }

    // Les méthodes setEnvUp() et tearEnvDown() serviront à terme à initialiser la
    // base de données
    // et les DAO

    // https://stackoverflow.com/questions/25906976/jerseytest-and-junit-throws-nullpointerexception
    @Before
    public void setEnvUp() {
      dao = BDDFactory.buildDao(PizzaDao.class);
      dao.createTable();
      dao.createAssocTable();
    }

    @After
    public void tearEnvDown() throws Exception {
       dao.dropTable();
    }


    @Test
    public void testGetEmptyList() {
        // La méthode target() permet de préparer une requête sur une URI.
        // La classe Response permet de traiter la réponse HTTP reçue.
        Response response = target("/pizzas").request().get();

        // On vérifie le code de la réponse (200 = OK)
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        // On vérifie la valeur retournée (liste vide)
        // L'entité (readEntity() correspond au corps de la réponse HTTP.
        // La classe jakarta.ws.rs.core.GenericType<T> permet de définir le type
        // de la réponse lue quand on a un type paramétré (typiquement une liste).
        List<PizzaDto> pizzas;
        pizzas = response.readEntity(new GenericType<List<PizzaDto>>() {
        });

        assertEquals(0, pizzas.size());

    }
    
    @Test
    public void testGetNotExistingIngredient() {
      Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().get();
      assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
    }
    
    @Test
    public void testGetExistingPizza() {

        Pizza pizza = new Pizza();
        pizza.setName("4 fromages");
        dao.insert(pizza);

        Response response = target("/pizzas").path(pizza.getId().toString()).request(MediaType.APPLICATION_JSON).get();

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
        assertEquals(pizza, result);
    }

    @Test
     public void testCreatePizza() {
         PizzaCreateDto ingredientCreateDto = new PizzaCreateDto();
         ingredientCreateDto.setName("Paysanne");

         Response response = target("/pizzas").request().post(Entity.json(ingredientCreateDto));

         assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

         PizzaDto returnedEntity = response.readEntity(PizzaDto.class);

         assertEquals(target("/pizzas/" + returnedEntity.getId()).getUri(), response.getLocation());
         assertEquals(returnedEntity.getName(), ingredientCreateDto.getName());
     }

     @Test
     public void testCreateSamePizza() {
         Pizza pizza = new Pizza();
         pizza.setName("4 fromages");
         dao.insert(pizza);

         PizzaCreateDto ingredientCreateDto = Pizza.toCreateDto(pizza);
         Response response = target("/pizzas").request().post(Entity.json(ingredientCreateDto));

         assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
     }

     @Test
     public void testCreatePizzaWithoutName() {
         PizzaCreateDto ingredientCreateDto = new PizzaCreateDto();

         Response response = target("/pizzas").request().post(Entity.json(ingredientCreateDto));

         assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
     }
     
     @Test
     public void testDeleteExistingPizza() {
       Pizza pizza = new Pizza();
       pizza.setName("4 fromages");
       dao.insert(pizza);

       Response response = target("/pizzas/").path(pizza.getId().toString()).request().delete();

       assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

       Pizza result = dao.findById(pizza.getId());
       assertEquals(result, null);
    }

    @Test
    public void testDeleteNotExistingPizza() {
      Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().delete();
      assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }

    @Test
    public void testGetPizzaName() {
      Pizza pizza = new Pizza();
      pizza.setName("Chorizo");
      dao.insert(pizza);

      Response response = target("pizzas").path(pizza.getId().toString()).path("name").request().get();

      assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

      assertEquals("Chorizo", response.readEntity(String.class));
   }

   @Test
   public void testGetNotExistingPizzaName() {
     Response response = target("pizzas").path(UUID.randomUUID().toString()).path("name").request().get();

     assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
   }
   
   @Test
   public void testCreateWithForm() {
       Form form = new Form();
       form.param("name", "4 fromages");

       Entity<Form> formEntity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
       Response response = target("pizzas").request().post(formEntity);

       assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
       String location = response.getHeaderString("Location");
       String id = location.substring(location.lastIndexOf('/') + 1);
       Pizza result = dao.findById(UUID.fromString(id));

       assertNotNull(result);
   }
}
